import React from 'react';
import { ThemeProvider } from '@material-ui/core/styles';
import { ContextProvider } from './components/context/AppContext';
import Visualizer from './components/ui';

import { createMuiTheme } from "@material-ui/core";

import './App.css';

const theme = createMuiTheme({});

const App: React.FC = () => {
  return (
    <ThemeProvider theme={theme}>
      <ContextProvider>
        <Visualizer />
      </ContextProvider>
    </ThemeProvider>
  );
}

export default App;
