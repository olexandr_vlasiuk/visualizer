import React from 'react';

import contextReducer, { createDefaultStorage, ContextState } from './Reducer';

import { ContextStateAction } from './Reducer/Actions';

export interface ContextProps {
  appContext: ContextState;
  appContextDispatch: React.Dispatch<ContextStateAction>;
}

const Context = React.createContext<ContextProps>({
  appContext: createDefaultStorage(),
  appContextDispatch: (action: ContextStateAction) => {
    throw new Error('Using Context without the provider');
  },
});

export const ContextProvider: React.FC = (props) => {
  const [appContext, appContextDispatch] = React.useReducer(contextReducer, createDefaultStorage());

  return <Context.Provider value={{ appContext, appContextDispatch }}>{props.children}</Context.Provider>;
};

export const withAppContext = (WrappedComponent: any) => (props: any) => {
  const { appContext, appContextDispatch } = React.useContext(Context);

  return <WrappedComponent {...props} appContext={appContext} appContextDispatch={appContextDispatch} />;
};
