import { Settings } from '../../models/settings';

export type ContextStateAction = { type: 'SET_SETTINGS'; settings: Settings };

export const setSettig = (settings: Settings): ContextStateAction => ({
  type: 'SET_SETTINGS',
  settings,
});
