import { Settings } from '../../models/settings';
import { ContextStateAction } from './Actions';

export interface ContextState {
  settings: Settings;
}

export const createDefaultStorage = (): ContextState => {
  return {
    ...createDefaultState(),
  };
};

export const createDefaultState = (): ContextState => ({
  settings: {}
});

const contextReducer = (state: ContextState, action: ContextStateAction) => {
  switch (action.type) {
    case 'SET_SETTINGS':
      return {
        ...state,
        settings: action.settings,
      };

    default:
      throw new Error('contextReducer: unknown action');
  }
};

export default contextReducer;
