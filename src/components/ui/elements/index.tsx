import React from 'react';

import tubB1 from '../../assets/tub/b1.jpeg';

const BASE_IMAGE_RATIO = {
  w: 2560,
  h: 2304
}

interface ElementsProps {
  bgEl: any;
  selectedItem: Array<{
    zIndex: number;
    position: {
      x: number,
      y: number
    };
    width: number;
    height: number;
    element: {
      image: string,
      name: string
    }
  }> | null;
}

const Elements: React.FC<ElementsProps> = (props: ElementsProps) => {
  return (
    <>
      {(!props.selectedItem?.length || !props.bgEl.current) ? <></> : (
        props.selectedItem?.map(selectedItem => {
          if (selectedItem === null) {
            return <></>;
          }
          return (
            <img src={selectedItem.element.image} style={{
              position: 'absolute',
              zIndex: selectedItem.zIndex,
              left: selectedItem.position.x * props.bgEl.current?.offsetHeight / BASE_IMAGE_RATIO.h,
              top: selectedItem.position.y * props.bgEl.current?.offsetWidth / BASE_IMAGE_RATIO.w,
              width: selectedItem.width * props.bgEl.current?.offsetWidth / BASE_IMAGE_RATIO.w,
              height: selectedItem.height * props.bgEl.current?.offsetHeight / BASE_IMAGE_RATIO.h,
            }} />
          )
        })
      )}
    </>
  )
}

export default Elements;