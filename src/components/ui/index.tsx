import React from 'react';
import MainCanvas from './mainCanvas';
import Menu from './menu';
import {
  Grid,
} from '@material-ui/core';

import { mapper } from './utils/imageMapper';

import { makeStyles } from '@material-ui/core/styles';

const DEFAULT_POSITION = {
  x: 0,
  y: 0
};

const BASE_IMAGE_RATIO = {
  w: 2560,
  h: 2304
}

const useStyles = makeStyles((theme: any) => ({
  root: {
      width: '100%',
      [theme.breakpoints.down('md')]: {
       flexDirection: 'column-reverse'
      },
  },
  
}));

const Visualizer: React.FC = () => {
  const classes = useStyles();

  const [selectedItem, setSelectedItem] = React.useState({
    Walls: {
      zIndex: 2,
      position: DEFAULT_POSITION,
      width: BASE_IMAGE_RATIO.w,
      height: BASE_IMAGE_RATIO.h,
      element: {
          image: mapper['Wall__Biscuit'],
          name: 'Wall Biscuit'
      },
    },
    Bases: {
      zIndex: 4,
      position: DEFAULT_POSITION,
      width: BASE_IMAGE_RATIO.w,
      height: BASE_IMAGE_RATIO.h,
      element: {
          image: mapper['Archer__Biscuit'],
          name: 'Archer Biscuit'
      },
    },
    Doors: {
      zIndex: 10,
      position: DEFAULT_POSITION,
      width: BASE_IMAGE_RATIO.w,
      height: BASE_IMAGE_RATIO.h,
      element: {
          image: mapper['Fluence__Chrome'],
          name: 'Fluence Chrome',
          key: 'Fluence__Chrome'
      },
    },
    Facets: {
      zIndex: 5,
      position: DEFAULT_POSITION,
      width: BASE_IMAGE_RATIO.w,
      height: BASE_IMAGE_RATIO.h,
      element: {
          image: mapper['Loure__Chrome'],
          name: 'Loure Chrome',
          key: 'Loure__Chrome'
      },
    }
  }) as any;
  const [, updateState] = React.useState() as any;
  const forceUpdate = React.useCallback(() => updateState({}), []);

  const addSelectedItem = (type: any, newItem: any) => {
    let doors = {};
    let showerLockers = {};
    let other = {};
    if(type === 'Bases') {
      // if(!newItem.element.key.includes('Shower')) {
      //   showerLockers = {
      //     Shower_Lockers: null
      //   }
      // }
      if(newItem.element.key.includes('Shower')) {
        other = {
          Other: null
        }
      }
      if(newItem.element.key.includes('Shower') || newItem.element.key.includes('Tresham')) {
        doors = {
          Doors: {
            ...selectedItem.Doors,
            element: {
              ...selectedItem.Doors.element,
              image: mapper[`${selectedItem.Doors.element.key}__s`]
            }
          }
        };
      } else {
        doors = {
          Doors: {
            ...selectedItem.Doors,
            element: {
              ...selectedItem.Doors.element,
              image: mapper[`${selectedItem.Doors.element.key}`]
            }
          }
        };
      }
    }
    setSelectedItem({
      ...selectedItem,
      [type]: newItem,
      ...doors,
      ...showerLockers,
      ...other
    });
    
  }

  React.useEffect(() => {
    window.addEventListener('resize', () => {
      forceUpdate();
    })
    setTimeout(() => {
      forceUpdate();
    }, 100)
    return () => {
      window.removeEventListener('resize', () => {
        forceUpdate();
      })
    }
  }, []);

  return (
    <Grid container className={classes.root} >
      <Grid xs={12} lg={6} >
        <MainCanvas selectedItem={selectedItem} />
      </Grid>
      <Grid xs={12} lg={6}>
        <Menu setSelectedItem={addSelectedItem} />
      </Grid>
    </Grid>
  );
}

export default Visualizer;