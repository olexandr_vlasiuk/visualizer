import React from 'react';
import {
  Grid,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import bathroomBase from '../../assets/bathroom-base.png';
import bathroomBase1 from '../../assets/bathroom-base1.png';

import Elements from '../elements';

const useStyles = makeStyles({
  root: {
    // maxWidth: '750px',
    // position: 'relative'
  },
  bgImageContainer: {
    width: 'inherit',
    position: 'relative',
    // zIndex: 1,
  },
  shadowContainer: {
    // width: '100%',
    // position: 'absolute',
    // zIndex: 100,
  },
  elementsContainer: {
    width: '100%',
    position: 'absolute',
    // zIndex: 100,
  },
  bgImage: {
    width: '100%',
  },
  hover: {
    '&:hover path': {
      fill: 'rgba(0,0,0,0.4)'
    }
  }
});

interface MainCanvasProps {
  selectedItem: any;
}

const MainCanvas: React.FC<MainCanvasProps> = (props) => {
  const classes = useStyles();
  const bgEl: any = React.useRef(null);

  // const [currentType, setCurrentType] = React.useState('');
  // const [selectedItem, setSelectedItem] = React.useState(null) as any;

  // const addSelectedItem = (type: any, newItem: any) => {
  //   setSelectedItem({
  //     ...selectedItem,
  //     [type]: newItem
  //   })
  // }
  return (
    <Grid className={classes.root}>
      <Grid className={classes.bgImageContainer}>
        <img className={classes.bgImage} src={bathroomBase1} style={{ position: 'absolute', zIndex: 1 }} alt="" ref={bgEl} />
        <img className={classes.bgImage} src={bathroomBase} style={{ position: 'absolute', zIndex: 3 }} alt="" ref={bgEl} />
      </Grid>
      <Grid className={classes.elementsContainer}>
        <Elements bgEl={bgEl} selectedItem={props.selectedItem !== null ? Object.values(props.selectedItem) : []} />
      </Grid>
    </Grid>
  )
}

export default MainCanvas;
