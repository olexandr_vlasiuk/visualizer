import React from 'react';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import { makeStyles } from '@material-ui/core/styles';

import Archer from '../../assets/ico/Items/Archer.png';
import Expanse from '../../assets/ico/Items/Expanse.png';
import Tresham from '../../assets/ico/Items/Tresham.png';
import Shower from '../../assets/ico/Items/Shower.png';
import Noimg from '../../assets/NO.png';

import Fluence from '../../assets/ico/Items/Fluence.png';
import Levity_H from '../../assets/ico/Items/Levity H.png';
import Levity_V from '../../assets/ico/Items/Levity V.png';
import Revel_H from '../../assets/ico/Items/Revel H.png';
import Revel_V from '../../assets/ico/Items/Revel V.png';

import { mapper } from '../utils/imageMapper';

const useStyles = makeStyles((theme: any) => ({
    root: {
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(20),
        fontWeight: theme.typography.fontWeightBold,
    },
    paper: {
        position: 'absolute',
        width: 700,
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
        borderRadius: 5,
        '&:focus': {
            outline: 'none',
        }
    },
    itemsBlock: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        maxHeight: 600,
        overflow: 'auto',
    },
    item: {
        maxWidth: 170,
        width: 170,
        margin: '15px',
        cursor: 'pointer',
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
        position: 'relative',
        padding: 5,
        transition: 'all .2s ease-in-out',
        boxShadow: '0 0 10px rgba(0,0,0,0.5)',
        borderRadius: 5,
        border: '1px solid white',
        '&:hover': {
            transform: 'scale(1.1)',
            border: '1px solid green',
        }
    },
    itemColor: {
        margin: 5,
        cursor: 'pointer',
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
        position: 'relative',
        padding: 5,
        width: 40,
        transition: 'all .2s ease-in-out',
        boxShadow: '0 0 10px rgba(0,0,0,0.5)',
        borderRadius: 5,
        border: '1px solid white',
        '&:hover': {
            transform: 'scale(1.1)',
            border: '1px solid green',
        }
    },
    itemColor_selected: {
        transform: 'scale(1.1)',
        border: '1px solid green',
    },
    item_selected: {
        transform: 'scale(1.1)',
        border: '1px solid green',
    },
    img: {
        width: '100%'
    },
    closeButton: {
        position: 'absolute',
        right: 0,
        top: 0,
        color: theme.palette.grey[500],
        fontSize: 15,
        zIndex: 999
    },
    imgTitle: {
        marginTop: 10
    },
    colorTitle: {
        fontSize: 10,
        marginTop: 5,
        textAlign: 'center'
    },
    colorBlock: {
        position: 'absolute',
        top: 5,
        right: 5,
    },
    colorItem: {
        width: 10,
        height: 10,
        margin: 1,
        padding: 1,
        marginBottom: 5,
        border: '1px solid #e0e0e0',
        '&:hover': {
            transform: 'scale(1.1)',
            boxShadow: '0 0 10px rgba(0,0,0,0.5)',
        }
    }
}));

interface MenuElementItem {
    key: string;
    name: string;
    color?: string;
    img?: string;
    add?: string;
}

interface MenuItem {
    key: string;
    name: string;
    types: MenuElementItem[];
    colors: MenuElementItem[];
}

const menu: MenuItem[] = [
    {
        key: 'Walls',
        name: 'Walls',
        types: [
            {
                key: 'Wall',
                name: 'Wall',
                color: '#fff',
            },
        ],
        colors: [
            {
                key: 'White_Brick',
                name: 'White Brick',
                img: mapper['White_Brick'],
            },
            {
                key: 'White',
                name: 'White',
                img: mapper['White'],
            },
            {
                key: 'VeinCut_Sandbar',
                name: 'Veincut Sandbar',
                img: mapper['VeinCut_Sandbar'],
            },
            {
                key: 'VeinCut_Dune',
                name: 'Veincut Dune',
                img: mapper['VeinCut_Dune'],
            },
            {
                key: 'CrossCut_Dune',
                name: 'Crosscut Dune',
                img: mapper['CrossCut_Dune'],
            },
            {
                key: 'Cintilante_Bluette',
                name: 'Cintilante Bluette',
                img: mapper['Cintilante_Bluette'],
            },
            {
                key: 'Calacatta_Crema',
                name: 'Calacatta Crema',
                img: mapper['Calacatta_Crema'],
            },
            {
                key: 'Biscuit',
                name: 'Biscuit',
                img: mapper['Biscuit'],
            },
        ]
    },
    {
        key: 'Bases',
        name: 'Bases',
        types: [
            {
                key: 'Archer',
                name: 'Archer',
                img: Archer,
            },
            {
                key: 'Expanse',
                name: 'Expanse',
                img: Expanse,
            },
            {
                key: 'Tresham',
                name: 'Tresham',
                img: Tresham,
                add: '__s',
            },
            {
                key: 'Shower',
                name: 'Shower',
                img: Shower,
                add: '__s',
            },
        ],
        colors: [
            {
                key: 'White',
                name: 'White',
                img: mapper['White'],
            },
            {
                key: 'Biscuit',
                name: 'Biscuit',
                img: mapper['Biscuit'],
            },
        ]
    },
    {
        key: 'Doors',
        name: 'Doors',
        types: [
            {
                key: 'Fluence',
                name: 'Fluence',
                img: Fluence,
            },
            {
                key: 'Levity_H',
                name: 'Levity (type 1)',
                img: Levity_H,
            },
            {
                key: 'Levity_V',
                name: 'Levity (type 2)',
                img: Levity_V,
            },
            {
                key: 'Revel_H',
                name: 'Revel (type 1)',
                img: Revel_H,
            },
            {
                key: 'Revel_V',
                name: 'Revel (type 2)',
                img: Revel_V,
            },
        ],
        colors: [
            {
                key: 'Chrome',
                name: 'Chrome',
                img: mapper['Chrome'],
            },
            {
                key: 'Nickel',
                name: 'Nickel',
                img: mapper['Nickel'],
            },
        ]
    }, 
    {
        key: 'Facets',
        name: 'Faucets',
        types: [
            {
                key: 'Alteo',
                name: 'Alteo',
                img: mapper['Alteo']
            },
            {
                key: 'Devonshire',
                name: 'Devonshire',
                img: mapper['Devonshire']
            },
            {
                key: 'Handshower',
                name: 'Handshower',
                img: mapper['Handshower']
            },
            {
                key: 'Loure',
                name: 'Loure',
                img: mapper['Loure']
            },
            {
                key: 'Hydro_Rail',
                name: 'Hydro Rail',
                img: mapper['Hydro_Rail']
            },
        ],
        colors: [
            {
                key: 'Chrome',
                name: 'Chrome',
                img: mapper['Chrome'],
            },
            {
                key: 'Nickel',
                name: 'Nickel',
                img: mapper['Nickel'],
            },
            {
                key: 'Bronze',
                name: 'Bronze',
                img: mapper['Bronze'],
            },
        ]
    },
    {
        key: 'Shower_Lockers',
        name: 'Shower Lockers',
        types: [
            {
                key: 'Shower_Locker',
                name: 'Shower Locker',
                img: mapper['Shower_Locker']
            }
        ],
        colors: [
            {
                key: 'White',
                name: 'White',
                img: mapper['White'],
            },
            {
                key: 'Biscuit',
                name: 'Biscuit',
                img: mapper['Biscuit'],
            },
            {
                key: 'Dune',
                name: 'Dune',
                img: mapper['Dune'],
            },
            {
                key: 'Sandbar',
                name: 'Sandbar',
                img: mapper['Sandbar'],
            },
        ]
    },
    {
        key: 'Other',
        name: 'Other',
        types: [
            {
                key: 'Shelf',
                name: 'Shelf (21")',
                img: mapper['Shelf']
            },
            {
                key: 'Shelf1',
                name: 'Shelf (14")',
                img: mapper['Shelf1']
            },
            {
                key: 'Shower_barre',
                name: 'Shower barre (54")',
                img: mapper['Shower_barre']
            },
            {
                key: 'Shower_barre1',
                name: 'Shower barre (36")',
                img: mapper['Shower_barre1']
            },
            {
                key: 'Shower_barre2',
                name: 'Shower barre (24")',
                img: mapper['Shower_barre2']
            },
            {
                key: 'Shower_teak_tray',
                name: 'Shower teak tray',
                img: mapper['shower_teak_tray']
            },
        ],
        colors: [
            {
                key: 'Chrome',
                name: 'Chrome',
                img: mapper['Chrome'],
            },
            {
                key: 'Nickel',
                name: 'Nickel',
                img: mapper['Nickel'],
            },
            {
                key: 'Bronze',
                name: 'Bronze',
                img: mapper['Bronze'],
            },
        ]
    }
]

interface MenuProps {
    setSelectedItem: any;
}

const DEFAULT_POSITION = {
    x: 0,
    y: 0
};

const BASE_IMAGE_RATIO = {
    w: 2560,
    h: 2304
}

const DISABLE_WALL = 'Walls';

const Menu: React.FC<MenuProps> = (props) => {
    const classes = useStyles();

    const [addItem, setAddItem] = React.useState<string>('');
    const [defaults, setDefaults] = React.useState<any>({
        Bases: {
            zIndex: 4,
            type: 'Archer',
            color: 'Biscuit'
        },
        Walls: {
            zIndex: 2,
            type: 'Wall',
            color: 'Biscuit'
        },
        Doors: {
            zIndex: 10,
            type: 'Fluence',
            color: 'Chrome'
        },
        Facets: {
            zIndex: 5,
            type: 'Alteo',
            color: 'Chrome'
        },
        Shower_Lockers: {
            zIndex: 3,
            type: '',
            color: 'White'
        },
        Other: {
            zIndex: 7,
            type: '',
            color: 'Chrome'
        },
    });

    return (
        <div style={{
            padding: 10,
            maxHeight: '100vh',
            overflow: 'auto'
        }}>
            <Typography style={{fontSize: 24, fontWeight: 700 }}>Please, select items for your bathroom:</Typography>
            <Typography style={{fontSize: 14}}>NOTE: type & color should be selected for displaying item.</Typography>
            {
                menu.map((menuItem: MenuItem) => {
                    // if(defaults.Bases.type !== 'Shower' && menuItem.key === 'Shower_Lockers') {
                    //     return <></>;
                    // }
                    return (
                        <Accordion style={{ width: '100%' }} key={`menuItem-${menuItem.key}`}>
                            <AccordionSummary
                                expandIcon={<ExpandMoreIcon />}
                                aria-controls="panel1a-content"
                                id="panel1a-header"
                            >
                                <Typography className={classes.heading}>{menuItem.name}</Typography>
                            </AccordionSummary>
                            <AccordionDetails>
                                <div style={{
                                    display: 'flex',
                                    flexDirection: 'column',
                                    width: '100%'
                                }}>
                                    {DISABLE_WALL !== menuItem.key && <div className={classes.itemsBlock}>
                                        {menuItem.types.map((type: MenuElementItem) => {
                                            if(defaults.Bases.type === 'Shower' && type.key.includes('Shower_barre')) {
                                                return <></>;
                                            }
                                            return (
                                            <div
                                                key={`type-${type.key}`}
                                                className={[classes.item, type.key === defaults[menuItem.key].type ? classes.item_selected : ''].join(' ')}
                                                onClick={() => {
                                                    setDefaults({
                                                        ...defaults,
                                                        [menuItem.key]: {
                                                            ...defaults[menuItem.key],
                                                            type: type.key
                                                        }
                                                    });
                                                    if (menuItem.key === 'Bases') {
                                                        setAddItem(type.add || '');
                                                    }
                                                    props.setSelectedItem(menuItem.key, {
                                                        zIndex: defaults[menuItem.key].zIndex,
                                                        position: DEFAULT_POSITION,
                                                        width: BASE_IMAGE_RATIO.w,
                                                        height: BASE_IMAGE_RATIO.h,
                                                        element: {
                                                            image: mapper[`${type.key}__${defaults[menuItem.key].color}${menuItem.key === 'Doors' ? addItem : ''}`],
                                                            name: `${type.key} ${defaults[menuItem.key].color}`,
                                                            key: `${type.key}__${defaults[menuItem.key].color}`
                                                        },
                                                    });
                                                }}
                                            >
                                                {type.color ? <span style={{
                                                    width: '110px',
                                                    height: '110px',
                                                    backgroundColor: type.color
                                                }} /> : <img src={type.img || ''} alt="" className={classes.img} />}
                                                <span className={classes.imgTitle}>{type.name}</span>
                                            </div>
                                        )})
                                        }
                                        <div
                                            onClick={() => {
                                                setDefaults({
                                                    ...defaults,
                                                    [menuItem.key]: {
                                                        ...defaults[menuItem.key],
                                                        type: ''
                                                    }
                                                });
                                                props.setSelectedItem(menuItem.key, {
                                                    zIndex: 0,
                                                    position: DEFAULT_POSITION,
                                                    width: BASE_IMAGE_RATIO.w,
                                                    height: BASE_IMAGE_RATIO.h,
                                                    element: {
                                                        image: '',
                                                        name: `${defaults[menuItem.key].type} `,
                                                        key: ''
                                                    },
                                                });
                                            }}
                                            className={[classes.item].join(' ')}
                                        >
                                            <img src={Noimg} alt='None' width={110} />
                                            <span className={classes.imgTitle}>Disable</span>
                                        </div>
                                    </div>}
                                    <Typography style={{
                                        margin: '10px 0',
                                        fontWeight: 700
                                    }}>Please, select a color for item:</Typography>
                                    <div style={{
                                        display: 'flex',
                                        justifyContent: 'center',
                                        flexWrap: 'wrap'
                                    }}>
                                        {
                                            menuItem.colors.map((color: MenuElementItem) => {
                                                if(menuItem.key === 'Other'&& defaults.Other.type === 'Shower_teak_tray') {
                                                    return <></>
                                                }
                                                return (
                                                <div
                                                    onClick={() => {
                                                        setDefaults({
                                                            ...defaults,
                                                            [menuItem.key]: {
                                                                ...defaults[menuItem.key],
                                                                color: color.key
                                                            }
                                                        });
                                                        props.setSelectedItem(menuItem.key, {
                                                            zIndex: defaults[menuItem.key].zIndex,
                                                            position: DEFAULT_POSITION,
                                                            width: BASE_IMAGE_RATIO.w,
                                                            height: BASE_IMAGE_RATIO.h,
                                                            element: {
                                                                image: mapper[`${defaults[menuItem.key].type}__${color.key}${menuItem.key === 'Doors' ? addItem : ''}`],
                                                                name: `${defaults[menuItem.key].type} ${color.key}`,
                                                                key: `${defaults[menuItem.key].type}__${color.key}`
                                                            },
                                                        });
                                                    }}
                                                    className={
                                                        [DISABLE_WALL !== menuItem.key ? classes.itemColor : classes.item, 
                                                            color.key === defaults[menuItem.key].color 
                                                            ? DISABLE_WALL !== menuItem.key ? classes.itemColor_selected : classes.item_selected : ''].join(' ')}
                                                    key={`color-${color.key}`}>
                                                    {color.color ? <span style={{
                                                        width: DISABLE_WALL !== menuItem.key ? '30px' : 110,
                                                        height: DISABLE_WALL !== menuItem.key ? '30px' : 110,
                                                        backgroundColor: color.color,
                                                        border: '1px solid grey'
                                                    }} /> : <img src={color.img} alt='' width={DISABLE_WALL !== menuItem.key ? 30 : 110} style={{
                                                        border: '1px solid grey'
                                                    }} />}
                                                    <span className={DISABLE_WALL !== menuItem.key ? classes.colorTitle : classes.imgTitle}>{color.name}</span>
                                                </div>
                                            )})
                                        }
                                        <div
                                            onClick={() => {
                                                setDefaults({
                                                    ...defaults,
                                                    [menuItem.key]: {
                                                        ...defaults[menuItem.key],
                                                        color: '',
                                                        key: ''
                                                    }
                                                });
                                                props.setSelectedItem(menuItem.key, {
                                                    zIndex: 0,
                                                    position: DEFAULT_POSITION,
                                                    width: BASE_IMAGE_RATIO.w,
                                                    height: BASE_IMAGE_RATIO.h,
                                                    element: {
                                                        image: '',
                                                        name: `${defaults[menuItem.key].type} `,
                                                        key: ''
                                                    },
                                                });
                                            }}
                                            className={DISABLE_WALL !== menuItem.key ? classes.itemColor : classes.item}
                                        >
                                            <img src={Noimg} alt='None' width={DISABLE_WALL !== menuItem.key ? 30 : 110} style={{
                                                border: '1px solid grey'
                                            }} />
                                            <span className={DISABLE_WALL !== menuItem.key ? classes.colorTitle : classes.imgTitle}>Disable</span>
                                        </div>
                                    </div>
                                </div>
                            </AccordionDetails>
                        </Accordion>
                    )
                })
            }
        </div>
    );
}

export default Menu;
