import React from 'react';
import paths from './paths';

import fabric from 'fabric';

interface ShadowProps {
  bgEl: any,
  setCurrentType: any;
}

const BASE_IMAGE_RATIO = {
  w: 2560,
  h: 2304
}

let canvas: any = null;

const Shadow: React.FC<ShadowProps> = (props: ShadowProps) => {

  const elementActions = () => {
    canvas.on('mouse:over', (e: any) => {
      if (e.target === null) return;
      e.target.set('fill', 'rgba(0, 0, 0, 0.5)');
      canvas.setActiveObject(e.target);
      canvas.renderAll();
    });

    canvas.on('mouse:up', (e: any) => {
      const activeObj = canvas.getActiveObject()
        if(activeObj && activeObj !== null) {
          activeObj.set('fill', false);
          props.setCurrentType(activeObj.customData.type);
          canvas.discardActiveObject();
        }
    });

    canvas.on('mouse:out', (e: any) => {
      if (e.target === null) return;
      e.target.set('fill', false);
      canvas.discardActiveObject();
      canvas.renderAll();
    });
  }

  const onPageResize = () => {
    const { bgEl } = props;

    canvas.setHeight(bgEl.current.offsetHeight);
    canvas.setWidth(bgEl.current.offsetWidth);
    canvas.getObjects().forEach((el: any) => {
      el.set({
        top: Math.floor(el.customData.positionStartY * bgEl.current.offsetHeight / BASE_IMAGE_RATIO.h),
        left: Math.floor(el.customData.positionStartX * bgEl.current.offsetWidth / BASE_IMAGE_RATIO.w),
        scaleY: canvas.height / BASE_IMAGE_RATIO.h,
        scaleX: canvas.width / BASE_IMAGE_RATIO.w,
      });
    })
    canvas.renderAll();
  };
  
  const canvasInit = React.useCallback(() => {
    const { bgEl } = props;
    canvas = new fabric.Canvas('shadows');

    paths.forEach(element => {
      const path = new fabric.Path(element.path, {
        fill: false,
        selectable: false,
        originX: 'left',
        originY: 'top',
        hasControls: false,
        hasBorders: false,
        lockMovementX: true,
        lockMovementY: true,
        customData: {
          positionStartX: element.basePoint.x,
          positionStartY: element.basePoint.y,
          type: element.type
        }
      });

      path.set({
        top: Math.floor(element.basePoint.y * bgEl.current.offsetHeight / BASE_IMAGE_RATIO.h),
        left: Math.floor(element.basePoint.x * bgEl.current.offsetWidth / BASE_IMAGE_RATIO.w),
        scaleY: bgEl.current.offsetHeight / BASE_IMAGE_RATIO.h,
        scaleX: bgEl.current.offsetWidth / BASE_IMAGE_RATIO.w,
      });

      // path.XZIndex = element.index;

      canvas.add(path);
    });

    canvas.setHeight(bgEl.current.offsetHeight);
    canvas.setWidth(bgEl.current.offsetWidth);
    canvas.renderAll();

    elementActions();
    window.addEventListener('resize', onPageResize);
  }, [ onPageResize, props ]);

  React.useEffect(() => {
    setTimeout(() => {
      canvasInit();
    }, 100);
    return () => {
      window.removeEventListener('resize', onPageResize);
    }
  }, []);

  return (
    <canvas id="shadows" />
  )
}

export default Shadow;
