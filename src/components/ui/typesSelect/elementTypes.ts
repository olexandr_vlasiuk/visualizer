import Archer_Biscuit from '../../assets/base/Archer_Biscuit.png';
import Archer_White from '../../assets/base/Archer_White.png';
import Expanse_Biscuit from '../../assets/base/Expanse_Biscuit.png';
import Expanse_White from '../../assets/base/Expanse_White.png';
import Shower_Biscuit from '../../assets/base/Shower_Biscuit.png';
import Shower_White from '../../assets/base/Shower_White.png';
import Tresham_Biscuit from '../../assets/base/Tresham_Biscuit.png';
import Tresham_White from '../../assets/base/Tresham_White.png';

import Wall_Biscuit from '../../assets/walls/Wall_Biscuit.png';
import Wall_Calacatta_Crema from '../../assets/walls/Wall_Calacatta_Crema.png';
import Wall_Cintilante_Bluette from '../../assets/walls/Wall_Cintilante_Bluette.png';
import Wall_CrossCut_Dune from '../../assets/walls/Wall_CrossCut_Dune.png';
import Wall_VeinCut_Dune from '../../assets/walls/Wall_VeinCut_Dune.png';
import Wall_VeinCut_Sandbar from '../../assets/walls/Wall_VeinCut_Sandbar.png';
import Wall_White from '../../assets/walls/Wall_White.png';
import Wall_White_Brick from '../../assets/walls/Wall_White_Brick.png';

import Fluence_Chrome from '../../assets/doors/b_Doors/Fluence_Chrome.png';
import Fluence_Nickel from '../../assets/doors/b_Doors/Fluence_Nickel.png';
import Levity_H_Chrome from '../../assets/doors/b_Doors/Levity_H_Chrome.png';
import Levity_H_Nickel from '../../assets/doors/b_Doors/Levity_H_Nickel.png';
import levity_V_Chrome from '../../assets/doors/b_Doors/levity_V_Chrome.png';
import levity_V_Nickel from '../../assets/doors/b_Doors/levity_V_Nickel.png';
import Revel_H_Chrome from '../../assets/doors/b_Doors/Revel_H_Chrome.png';
import Revel_H_Nickel from '../../assets/doors/b_Doors/Revel_H_Nickel.png';
import Revel_V_Chrome from '../../assets/doors/b_Doors/Revel_V_Chrome.png';
import Revel_V_Nickel from '../../assets/doors/b_Doors/Revel_V_Nickel.png';

import Fluence_Chrome_s from '../../assets/doors/s_Doors/Fluence_Chrome.png';
import Fluence_Nickel_s from '../../assets/doors/s_Doors/Fluence_Nickel.png';
import Levity_H_Chrome_s from '../../assets/doors/s_Doors/Levity_H_Chrome.png';
import Levity_H_Nickel_s from '../../assets/doors/s_Doors/Levity_H_Nickel.png';
import levity_V_Chrome_s from '../../assets/doors/s_Doors/levity_V_Chrome.png';
import levity_V_Nickel_s from '../../assets/doors/s_Doors/levity_V_Nickel.png';
import Revel_H_Chrome_s from '../../assets/doors/s_Doors/Revel_H_Chrome.png';
import Revel_H_Nickel_s from '../../assets/doors/s_Doors/Revel_H_Nickel.png';
import Revel_V_Chrome_s from '../../assets/doors/s_Doors/Revel_V_Chrome.png';
import Revel_V_Nickel_s from '../../assets/doors/s_Doors/Revel_V_Nickel.png';

const variants: any = {
  tub: {
    variants: {
      1: {
        image: Archer_Biscuit,
        name: 'Archer Biscuit'
      },
      2: {
        image: Archer_White,
        name: 'Archer White'
      },
      3: {
        image: Expanse_Biscuit,
        name: 'Expanse Biscuit'
      },
      4: {
        image: Expanse_White,
        name: 'Expanse White'
      },
      5: {
        image: Shower_Biscuit,
        name: 'Shower Biscuit'
      },
      6: {
        image: Shower_White,
        name: 'Shower White'
      },
      7: {
        image: Tresham_Biscuit,
        name: 'Tresham Biscuit'
      },
      8: {
        image: Tresham_White,
        name: 'Tresham White'
      },
    },
    zIndex: 3,
    position: {
      x: 0,
      y: 0
    },
    type: 1,
    width: 2560,
    height: 2304,
  },
  wall: {
    type: 2,
    position: {
      x: 0,
      y: 0
    },
    width: 2560,
    zIndex: 2,
    height: 2304,
    categories: {
      bgWall: {
        zIndex: 2,
        name: 'Background Wall',
        variants: {
          1: {
            image: Wall_Biscuit,
            name: 'Biscuit'
          },
          2: {
            image: Wall_Calacatta_Crema,
            name: 'Calacatta Crema'
          },
          3: {
            image: Wall_Cintilante_Bluette,
            name: 'Cintilante Bluette'
          },
          4: {
            image: Wall_CrossCut_Dune,
            name: 'CrossCut Dune'
          },
          5: {
            image: Wall_VeinCut_Dune,
            name: 'VeinCut Dune'
          },
          6: {
            image: Wall_VeinCut_Sandbar,
            name: 'VeinCut Sandbar'
          },
          7: {
            image: Wall_White,
            name: 'White'
          },
          8: {
            image: Wall_White_Brick,
            name: 'White Brick'
          },
        },
      },
      door: {
        name: 'Door',
        zIndex: 10,
        variants: {
          1: {
            image: Fluence_Chrome,
            name: 'Fluence Chrome (small)'
          },
          2: {
            image: Fluence_Chrome_s,
            name: 'Fluence Chrome'
          },
          3: {
            image: Fluence_Nickel,
            name: 'Fluence Nickel (small)'
          },
          4: {
            image: Fluence_Nickel_s,
            name: 'Fluence Nickel'
          },
          5: {
            image: Levity_H_Chrome,
            name: 'Levity H Chrome (small)'
          },
          6: {
            image: Levity_H_Chrome_s,
            name: 'Levity H Chrome'
          },
          7: {
            image: Levity_H_Nickel,
            name: 'Levity H Nickel (small)'
          },
          8: {
            image: Levity_H_Nickel_s,
            name: 'Levity H Nickel'
          },
          9: {
            image: levity_V_Chrome,
            name: 'levity V Chrome (small)'
          },
          10: {
            image: levity_V_Chrome_s,
            name: 'levity V Chrome'
          },
          11: {
            image: levity_V_Nickel,
            name: 'levity V Nickel (small)'
          },
          12: {
            image: levity_V_Nickel_s,
            name: 'levity V Nickel'
          },
          13: {
            image: Revel_H_Chrome,
            name: 'Revel H Chrome (small)'
          },
          14: {
            image: Revel_H_Chrome_s,
            name: 'Revel H Chrome'
          },
          15: {
            image: Revel_H_Nickel,
            name: 'Revel H Nickel (small)'
          },
          16: {
            image: Revel_H_Nickel_s,
            name: 'Revel H Nickel'
          },
          17: {
            image: Revel_V_Chrome,
            name: 'Revel V Chrome (small)'
          },
          18: {
            image: Revel_V_Chrome_s,
            name: 'Revel V Chrome'
          },
          19: {
            image: Revel_V_Nickel,
            name: 'Revel V Nickel (small)'
          },
          20: {
            image: Revel_V_Nickel_s,
            name: 'Revel V Nickel'
          },
        }
      }
    },
  }
};

export default variants;
