import React from 'react';
import {
  Modal,
  IconButton
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import variants from './elementTypes';

const useStyles = makeStyles((theme: any) => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  paper: {
    position: 'absolute',
    width: 700,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    borderRadius: 5,
    '&:focus': {
      outline: 'none',
    }
  },
  itemsBlock: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    maxHeight: 600,
    overflow: 'auto',
  },
  item: {
    maxWidth: 150,
    width: 150,
    margin: '15px 0',
    cursor: 'pointer',
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    position: 'relative',
    padding: 5,
    transition: 'all .2s ease-in-out',
    boxShadow: '0 0 10px rgba(0,0,0,0.5)',
    borderRadius: 5,
    '&:hover': {
      transform: 'scale(1.1)',
      border: '1px solid green',
    }
  },
  img: {
    width: '100%'
  },
  closeButton: {
    position: 'absolute',
    right: 0,
    top: 0,
    color: theme.palette.grey[500],
    fontSize: 15,
    zIndex: 999
  },
  imgTitle: {
  },
  colorBlock: {
    position: 'absolute',
    top: 5,
    right: 5,
  },
  colorItem: {
    width: 10,
    height: 10,
    margin: 1,
    padding: 1,
    marginBottom: 5,
    border: '1px solid #e0e0e0',
    '&:hover': {
      transform: 'scale(1.1)',
      boxShadow: '0 0 10px rgba(0,0,0,0.5)',
    }
  }
}));

interface TypesSelectProps {
  type: string;
  setCurrentType: any;
  setSelectedItem: any;
}

function getModalStyle() {
  return {
    top: `50%`,
    left: `50%`,
    transform: `translate(-50%, -50%)`,
  };
}

const TypesSelect: React.FC<TypesSelectProps> = (props: TypesSelectProps) => {
  const classes = useStyles();
  const [modalStyle] = React.useState(getModalStyle);
  const [variant2Els, setVariant2Els] = React.useState({}) as any;

  const onClose = React.useCallback(() => {
    props.setCurrentType('')
  }, []);

  React.useEffect(() => {

    // if (variants[props.type]?.type === 2) {
    //   let els: any = {};

    //   Object.values(variants[props.type].variants).forEach(({ name }: any) => {
    //     els = {
    //       ...els,
    //       [name]: {
    //         image: variants[props.type].images[`${name}_${Object.values(variants[props.type].colors)[0]}`],
    //         name
    //       }
    //     }
    //   })
    //   setVariant2Els(els);
    // }
  }, [props.type]);

  const renderType1 = (elements: any) => {
    return (
      elements.map((el: any) => (
        <div
          key={`${props.type}-${el[0]}`}
          className={classes.item}
          onClick={() => {
            props.setSelectedItem(props.type, {
              zIndex: variants[props.type].zIndex,
              position: variants[props.type].position,
              width: variants[props.type].width,
              height: variants[props.type].height,
              element: el[1],
            });
            onClose();
          }}
        >
          <img src={el[1].image} alt="" className={classes.img} />
          <span className={classes.imgTitle}>{el[1].name}</span>
        </div>
      ))
    )
  }

  const handleColorHover = (name: string, color: string) => {
    const updatedRenders: any = {
      ...variant2Els,
      [name]: {
        ...variant2Els[name],
        image: variants[props.type].images[`${name}_${color}`],
      }
    }
    setVariant2Els(updatedRenders);

  }
  const renderType2 = () => {
    return (
      <div style={{
        display: 'flex',
        flexDirection: 'column',
        width: '100%'
      }}>
        {Object.entries(variants[props?.type]?.categories).map((el: any) => {
          return (
            <Accordion style={{ width: '100%' }}>
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1a-content"
                id="panel1a-header"
              >
                <Typography className={classes.heading}>{el[1].name}</Typography>
              </AccordionSummary>
              <AccordionDetails>
                <div className={classes.itemsBlock}>
                  {Object.entries(el[1].variants).map((variant: any) => (
                    <div
                      key={`${props.type}-${variant[0]}`}
                      className={classes.item}
                      onClick={() => {
                        props.setSelectedItem(el[0], {
                          zIndex: el[1].zIndex,
                          position: variants[props.type].position,
                          width: variants[props.type].width,
                          height: variants[props.type].height,
                          element: variant[1],
                        });
                        onClose();
                      }}
                    >
                      <img src={variant[1].image} alt="" className={classes.img} />
                      <span className={classes.imgTitle}>{variant[1].name}</span>
                    </div>
                  ))
                  }
                </div>
              </AccordionDetails>
            </Accordion>
          )
        })}
      </div>
    )
  }

  return (
    <Modal
      open={!!props.type}
      onClose={onClose}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
    >
      <div style={modalStyle} className={classes.paper}>
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          &#10006;
        </IconButton>
        <div className={classes.itemsBlock}>
          {
            props.type && variants[props.type]?.type === 1 && renderType1(Object.entries(variants[props.type].variants))
          }
          {
            props.type && variants[props?.type]?.type === 2 && renderType2()
          }
        </div>
      </div>
    </Modal>
  )
}

export default TypesSelect;
