
import Archer__Biscuit from '../../assets/base/Archer_Biscuit.png';
import Archer__White from '../../assets/base/Archer_White.png';
import Expanse__Biscuit from '../../assets/base/Expanse_Biscuit.png';
import Expanse__White from '../../assets/base/Expanse_White.png';
import Shower__Biscuit from '../../assets/base/Shower_Biscuit.png';
import Shower__White from '../../assets/base/Shower_White.png';
import Tresham__Biscuit from '../../assets/base/Tresham_Biscuit.png';
import Tresham__White from '../../assets/base/Tresham_White.png';

import Wall__Biscuit from '../../assets/walls/Wall_Biscuit.png';
import Wall__Calacatta_Crema from '../../assets/walls/Wall_Calacatta_Crema.png';
import Wall__Cintilante_Bluette from '../../assets/walls/Wall_Cintilante_Bluette.png';
import Wall__CrossCut_Dune from '../../assets/walls/Wall_CrossCut_Dune.png';
import Wall__VeinCut_Dune from '../../assets/walls/Wall_VeinCut_Dune.png';
import Wall__VeinCut_Sandbar from '../../assets/walls/Wall_VeinCut_Sandbar.png';
import Wall__White from '../../assets/walls/Wall_White.png';
import Wall__White_Brick from '../../assets/walls/Wall_White_Brick.png';

import Biscuit from '../../assets/ico/Colors/Biscuit.jpg';
import Calacatta_Crema from '../../assets/ico/Colors/Calacatta_Crema.jpg';
import Cintilante_Bluette from '../../assets/ico/Colors/Cintilante_Bluette.jpg';
import CrossCut_Dune from '../../assets/ico/Colors/CrossCut_Dune.jpg';
import Dune from '../../assets/ico/Colors/CrossCut_Dune.jpg';
import OilRubbed_Bronze from '../../assets/ico/Colors/Bronze.jpg';
import Polished_Chrome from '../../assets/ico/Colors/Chrome.jpg';
import VeinCut_Dune from '../../assets/ico/Colors/VeinCut_Dune.jpg';
import VeinCut_Sandbar from '../../assets/ico/Colors/VeinCut_Sandbar.jpg';
import Sandbar from '../../assets/ico/Colors/VeinCut_Sandbar.jpg';
import Vibrant_Brushed_Nickel from '../../assets/ico/Colors/Nickel.jpg';
import White from '../../assets/ico/Colors/White.jpg';
import Chrome from '../../assets/ico/Colors/Chrome.jpg';
import Nickel from '../../assets/ico/Colors/Nickel.jpg';
import Bronze from '../../assets/ico/Colors/Bronze.jpg';
import White_Brick from '../../assets/ico/Colors/White_Brick.jpg';

import Fluence__Chrome from '../../assets/doors/b_Doors/Fluence_Chrome.png';
import Fluence__Nickel from '../../assets/doors/b_Doors/Fluence_Nickel.png';
import Levity_H__Chrome from '../../assets/doors/b_Doors/Levity_H_Chrome.png';
import Levity_H__Nickel from '../../assets/doors/b_Doors/Levity_H_Nickel.png';
import Levity_V__Chrome from '../../assets/doors/b_Doors/levity_V_Chrome.png';
import Levity_V__Nickel from '../../assets/doors/b_Doors/levity_V_Nickel.png';
import Revel_H__Chrome from '../../assets/doors/b_Doors/Revel_H_Chrome.png';
import Revel_H__Nickel from '../../assets/doors/b_Doors/Revel_H_Nickel.png';
import Revel_V__Chrome from '../../assets/doors/b_Doors/Revel_V_Chrome.png';
import Revel_V__Nickel from '../../assets/doors/b_Doors/Revel_V_Nickel.png';

import Fluence__Chrome__s from '../../assets/doors/s_Doors/Fluence_Chrome.png';
import Fluence__Nickel__s from '../../assets/doors/s_Doors/Fluence_Nickel.png';
import Levity_H__Chrome__s from '../../assets/doors/s_Doors/Levity_H_Chrome.png';
import Levity_H__Nickel__s from '../../assets/doors/s_Doors/Levity_H_Nickel.png';
import Levity_V__Chrome__s from '../../assets/doors/s_Doors/levity_V_Chrome.png';
import Levity_V__Nickel__s from '../../assets/doors/s_Doors/levity_V_Nickel.png';
import Revel_H__Chrome__s from '../../assets/doors/s_Doors/Revel_H_Chrome.png';
import Revel_H__Nickel__s from '../../assets/doors/s_Doors/Revel_H_Nickel.png';
import Revel_V__Chrome__s from '../../assets/doors/s_Doors/Revel_V_Chrome.png';
import Revel_V__Nickel__s from '../../assets/doors/s_Doors/Revel_V_Nickel.png';

import Alteo__Bronze from '../../assets/Facets/Alteo_Bronze.png';
import Alteo__Chrome from '../../assets/Facets/Alteo_Chrome.png';
import Alteo__Nickel from '../../assets/Facets/Alteo_Nickel.png';
import Devonshire__Bronze from '../../assets/Facets/Devonshire_Bronze.png';
import Devonshire__Chrome from '../../assets/Facets/Devonshire_Chrome.png';
import Devonshire__Nickel from '../../assets/Facets/Devonshire_Nickel.png';
import Handshower__Bronze from '../../assets/Facets/Handshower_Bronze.png';
import Handshower__Chrome from '../../assets/Facets/Handshower_Chrome.png';
import Handshower__Nickel from '../../assets/Facets/Handshower_Nickel.png';
import Hydro_Rail__Bronze from '../../assets/Facets/Hydro_Rail_Bronze.png';
import Hydro_Rail__Chrome from '../../assets/Facets/Hydro_Rail_Chrome.png';
import Hydro_Rail__Nickel from '../../assets/Facets/Hydro_Rail_Nickel.png';
import Loure__Bronze from '../../assets/Facets/Loure_Bronze.png';
import Loure__Chrome from '../../assets/Facets/Loure_Chrome.png';
import Loure__Nickel from '../../assets/Facets/Loure_Nickel.png';

import Alteo from '../../assets/ico/Items/Alteo.png';
import Devonshire from '../../assets/ico/Items/Devonshire.png';
import Handshower from '../../assets/ico/Items/Handshower.png';
import Hydro_Rail from '../../assets/ico/Items/Hydro Rail.png';
import Loure from '../../assets/ico/Items/Loure.png';
import Shelf from '../../assets/ico/Items/Shelf.png';
import Shelf1 from '../../assets/ico/Items/Shelf1.png';
import Shower_barre from '../../assets/ico/Items/Shower barre.png';
import Shower_barre1 from '../../assets/ico/Items/Shower barre1.png';
import Shower_barre2 from '../../assets/ico/Items/Shower barre2.png';
import shower_teak_tray from '../../assets/ico/Items/shower teak tray.png';

import Shower_Locker from '../../assets/ico/Items/Shower Locker.png';
import Shower_Locker__Biscuit from '../../assets/Shower Locker/Shower_Locker_Biscuit.png';
import Shower_Locker__Dune from '../../assets/Shower Locker/Shower_Locker_Dune.png';
import Shower_Locker__Sandbar from '../../assets/Shower Locker/Shower_Locker_Sandbar.png';
import Shower_Locker__White from '../../assets/Shower Locker/Shower_Locker_White.png';

import Shelf__Bronze from '../../assets/Access/Shelf_Bronze.png';
import Shelf__Chrome from '../../assets/Access/Shelf_Chrome.png';
import Shelf__Nickel from '../../assets/Access/Shelf_Nickel.png';
import Shelf1__Bronze from '../../assets/Access/Shelf1_Bronze.png';
import Shelf1__Chrome from '../../assets/Access/Shelf1_Chrome.png';
import Shelf1__Nickel from '../../assets/Access/Shelf1_Nickel.png';
import Shower_teak_tray from '../../assets/Access/shower teak tray.png';
import Shower_teak_tray__ from '../../assets/Access/shower teak tray.png';
import Shower_teak_tray__Bronze from '../../assets/Access/shower teak tray.png';
import Shower_teak_tray__Chrome from '../../assets/Access/shower teak tray.png';
import Shower_teak_tray__Nickel from '../../assets/Access/shower teak tray.png';
import Shower_barre__Bronze from '../../assets/Access/Shower_barre_Bronze.png';
import Shower_barre__Chrome from '../../assets/Access/Shower_barre_Chrome.png';
import Shower_barre__Nickel from '../../assets/Access/Shower_barre_Nickel.png';
import Shower_barre1__Bronze from '../../assets/Access/Shower_barre1_Bronze.png';
import Shower_barre1__Chrome from '../../assets/Access/Shower_barre1_Chrome.png';
import Shower_barre1__Nickel from '../../assets/Access/Shower_barre1_Nickel.png';
import Shower_barre2__Bronze from '../../assets/Access/Shower_barre2_Bronze.png';
import Shower_barre2__Chrome from '../../assets/Access/Shower_barre2_Chrome.png';
import Shower_barre2__Nickel from '../../assets/Access/Shower_barre2_Nickel.png';

const mapper: any = {
    shower_teak_tray,
    Shower_teak_tray__Bronze,
    Shower_teak_tray__Chrome,
    Shower_teak_tray__Nickel,
    Shower_teak_tray__,
    Hydro_Rail,
    Shelf,
    Shelf1,
    Shower_barre,
    Shower_barre1,
    Shower_barre2,
    Shower_Locker,
    Shower_Locker__Biscuit,
    Shower_Locker__Dune,
    Shower_Locker__Sandbar,
    Shower_Locker__White,
    Sandbar,
    Alteo,
    Devonshire,
    Handshower,
    Loure,
    Wall__Biscuit,
    Wall__Calacatta_Crema,
    Wall__Cintilante_Bluette,
    Wall__CrossCut_Dune,
    Wall__VeinCut_Dune,
    Wall__VeinCut_Sandbar,
    Wall__White,
    Wall__White_Brick,
    Biscuit,
    Chrome,
    Nickel,
    Calacatta_Crema,
    Cintilante_Bluette,
    CrossCut_Dune,
    OilRubbed_Bronze,
    Bronze,
    Polished_Chrome,
    VeinCut_Dune,
    VeinCut_Sandbar,
    Vibrant_Brushed_Nickel,
    White,
    White_Brick,
    Archer__Biscuit,
    Archer__White,
    Expanse__Biscuit,
    Expanse__White,
    Shower__Biscuit,
    Shower__White,
    Tresham__Biscuit,
    Tresham__White,

    Fluence__Chrome,
    Fluence__Nickel,
    Levity_H__Chrome,
    Levity_H__Nickel,
    Levity_V__Chrome,
    Levity_V__Nickel,
    Revel_H__Chrome,
    Revel_H__Nickel,
    Revel_V__Chrome,
    Revel_V__Nickel,

    Fluence__Chrome__s,
    Fluence__Nickel__s,
    Levity_H__Chrome__s,
    Levity_H__Nickel__s,
    Levity_V__Chrome__s,
    Levity_V__Nickel__s,
    Revel_H__Chrome__s,
    Revel_H__Nickel__s,
    Revel_V__Chrome__s,
    Revel_V__Nickel__s,

    Alteo__Bronze,
    Alteo__Chrome,
    Alteo__Nickel,
    Devonshire__Bronze,
    Devonshire__Chrome,
    Devonshire__Nickel,
    Handshower__Bronze,
    Handshower__Chrome,
    Handshower__Nickel,
    Hydro_Rail__Bronze,
    Hydro_Rail__Chrome,
    Hydro_Rail__Nickel,
    Loure__Bronze,
    Loure__Chrome,
    Loure__Nickel,
    Dune,

    Shelf__Bronze,
    Shelf__Chrome,
    Shelf__Nickel,
    Shelf1__Bronze,
    Shelf1__Chrome,
    Shelf1__Nickel,
    Shower_teak_tray,
    Shower_barre__Bronze,
    Shower_barre__Chrome,
    Shower_barre__Nickel,
    Shower_barre1__Bronze,
    Shower_barre1__Chrome,
    Shower_barre1__Nickel,
    Shower_barre2__Bronze,
    Shower_barre2__Chrome,
    Shower_barre2__Nickel,
}
export {
    mapper
}