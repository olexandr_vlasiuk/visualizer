const HtmlWebPackPlugin = require('html-webpack-plugin');
const path = require('path');
module.exports = {
	context: __dirname,
	entry: './src/index.js',
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'main.js',
		publicPath: '/',
	},
	devServer: {
		historyApiFallback: true
	},
	module: {
		rules: [
			{
				test: /\.ts[x]?$/,
				loader: 'awesome-typescript-loader',
			},
			{
				test: /\.js$/,
				use: 'babel-loader',
			},
			{
				test: /\.css$/,
				use: ['style-loader', 'css-loader'],
			},
			{
				test: /\.(png|svg|gif|jpg|jpeg)?$/,
				use: 'file-loader',
			},
			{
				test: /(fabric(\.min)?\.js|fabric)$/,
				use: 'exports-loader?fabric',
			},
		]
	},
	resolve: {
		extensions: ['.ts', '.tsx', '.js', '.jsx'],
	},
	plugins: [
		new HtmlWebPackPlugin({
			template: path.resolve(__dirname, 'public/index.html'),
			filename: 'index.html'
		})
	]
};